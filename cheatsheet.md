Command Table

| command                        | description                                   |
|--------------------------------+-----------------------------------------------|
| git status                     | shows all tracked updates                     |
| git checkout -b [branch]       | Create and swap branches                      |
| git checkout [branch]          | Swap Branches                                 |
| git diff [path/to]             | Show changes to file                          |
| git add [path/to]              | Adds file to next commit                      |
| git add --all                  | Adds all tracked files to next commit         |
| git commit -m "[comment]"      | Commits tracked files                         |
| git checkout -- [path/to]      | Removes file from next commit                 |
| git push -u origin [branch]    | Push working branch to remote as a branch     |
| git pull [ssh url]             | Pulls updates & new code to working directory |
| git pull -b [branch] [ssh url] | Pulls remote branch to working directory      |
| git clone [ssh url]            | Clone remote repo                             |
| git rebase master              | Rebases everything in master into your branch |


General Workflow
* Pull lastest code -> git pull [ssh url] or git clone [ssh url] for new start -> example ssh url: git@gitlab.com:chrisweaver1/git-cheatsheet.git (found on gitlab page)
* Make new branch or switch branch -> git checkout <-b> [branch]
* Write new code
* Show updated files and changes -> git status
* Check updates -> git diff [path]
* Add/remove files from commit -> git add [path], git checkout -- [path] -> this will revert changes made
* Check updates again -> git status -> files being committed will be highlighted green
* If happy -> git commit -m "[comment]"
* When ready, push to remote branch -> git push -u origin [branch]
* Merge request when code is tested and ready -> do this online, goto main repo -> click branches -> your branch -> merge request -> Squash commits if needed -> assign to Paul/Mark 
